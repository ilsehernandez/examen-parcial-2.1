﻿namespace ExamenParcial2._1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.trckBr1 = new System.Windows.Forms.TrackBar();
            this.trckBr2 = new System.Windows.Forms.TrackBar();
            this.lbl1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.trckBr1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trckBr2)).BeginInit();
            this.SuspendLayout();
            // 
            // trckBr1
            // 
            this.trckBr1.Location = new System.Drawing.Point(176, 89);
            this.trckBr1.Name = "trckBr1";
            this.trckBr1.Size = new System.Drawing.Size(509, 90);
            this.trckBr1.TabIndex = 0;
            this.trckBr1.Scroll += new System.EventHandler(this.trackBar1_Scroll);
            // 
            // trckBr2
            // 
            this.trckBr2.Location = new System.Drawing.Point(176, 212);
            this.trckBr2.Name = "trckBr2";
            this.trckBr2.Size = new System.Drawing.Size(252, 90);
            this.trckBr2.TabIndex = 1;
            this.trckBr2.Scroll += new System.EventHandler(this.trackBar1_Scroll);
            // 
            // lbl1
            // 
            this.lbl1.AutoSize = true;
            this.lbl1.Location = new System.Drawing.Point(227, 364);
            this.lbl1.Name = "lbl1";
            this.lbl1.Size = new System.Drawing.Size(70, 25);
            this.lbl1.TabIndex = 2;
            this.lbl1.Text = "label1";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(812, 564);
            this.Controls.Add(this.lbl1);
            this.Controls.Add(this.trckBr2);
            this.Controls.Add(this.trckBr1);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.trckBr1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trckBr2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TrackBar trckBr1;
        private System.Windows.Forms.TrackBar trckBr2;
        private System.Windows.Forms.Label lbl1;
    }
}

